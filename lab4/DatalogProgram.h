#include "Rule.h"

class DatalogProgram{
public:
	//class members
	vector<Predicate> schemes;
	vector<Predicate> facts;
	vector<Predicate> queries;
	vector<Rule> rules;

	DatalogProgram(){}
	string toString(){
		stringstream output;
		output << prediString(schemes) << prediString(facts) << rulesString() << prediString(queries);
		return output.str();
	}

	string prediString(vector<Predicate>& prediList){
		stringstream output;
		if (prediList[0].parameterList.size() != 0)
			output<<prediList[0].name<<"("<<prediList.size()<<"):"<<endl;
		else{
			output << prediList[0].getName() << "(0):" << endl;
			return output.str();
		}

		for(unsigned int i = 0;i<prediList.size();i++)
			output<<"  "<<prediList[i].toString()<<endl;
		return output.str();
	}

	string rulesString(){
		stringstream output;
		output << "Rules" << "(" << rules.size() << "):" << endl;
		for (unsigned int i = 0; i < rules.size(); i++)
			output << "  " << rules[i].toString() << endl;
		return output.str();
	}

	void addScheme(Predicate scheme){
		schemes.push_back(scheme);
	}

	void addFact(Predicate fact){
		facts.push_back(fact);
	}

	void addQuery(Predicate query){
		queries.push_back(query);
	}

	void addRule(Rule rule){
		rules.push_back(rule);
	}

	vector<Predicate>& getSchemes(){
		return schemes;
	}

	vector<Predicate>& getFacts(){
		return facts;
	}

	vector<Predicate>& getQueries(){
		return queries;
	}

	vector<Rule>& getRules(){
		return rules;
	}

	~DatalogProgram(){}
	/*void addScheme(Predicate scheme);
	void addFact(Predicate fact);
	void addQuery(Predicate query);
	void addRule(Rule rule);	
	schemeList getSchemes();
	factList getFacts();
	queryList getQueries();
	ruleList getRules();
	~DatalogProgram();*/
};