#include "database.h"

database::database(DatalogProgram newData){
	data = newData;
}

void database::evalSchemes(DatalogProgram data){
	for(unsigned int i=0;i<data.getSchemes().getList().size();i++){
		relation newRel;
		newRel.setName(data.getSchemes().getList()[i].getScheme().getID());
		newRel.setSchema(data.getSchemes().getList()[i].getScheme().getParamList());
		this->insert(pair<string,relation>(newRel.getName(),newRel));
	}
}

void database::insert(pair<string,relation> addMe){
	relations.insert(addMe);
}

void database::evalFacts(DatalogProgram data){
	for(unsigned int i=0;i<data.getFacts().getList().size();i++)
		if(data.getFacts().getList()[0].getFact().getID() != "")
			relations.at(data.getFacts().getList()[i].getFact().getID()).loadFacts(data.getFacts().getList()[i].getFact().getParamList());
}

void database::evalQueries(){
	vector<relation> selects;
	vector<relation> renames;
	vector<relation> projects;
	for(unsigned int i=0;i<data.getQueries().getList().size();i++){
		selects.push_back(relations.at(data.getQueries().getList()[i].getQuery().getID()).select(data.getQueries().getList()[i].getQuery().getParamList()));		//Select
		renames.push_back(selects[i].rename(data.getQueries().getList()[i].getQuery().getParamList()));					//Rename
		projects.push_back(renames[i].project(data.getQueries().getList()[i].getQuery().getParamList()));					//Project
	}
	results = projects;
}

DatalogProgram database::getData(){
	return data;	
}

string database::toString(){
	stringstream ss;
	for(unsigned int i = 0; i < data.getQueries().getList().size(); i++)
		ss<<data.getQueries().getList()[i].toString()<<"? "<<results[i].toString();
	return ss.str();
}

vector<relation> database::getResults(){
	return results;
}

database::~database(){}