#include "Parameter.h"

class Predicate
{

private:
	
	string identifier;
	string name;
	string Lparen, Rparen;
	string q_mark;
	vector<Parameter> parameterList;

public:

	Predicate();
	void setName(string _name);
	void setID(string _id);
	void setPunc(string punc);
	void addParam(Parameter param);
	string getID();
	string getName();
	string getQmark();
	string getLparen();
	string getRparen();
	vector<Parameter> getParamList();
	vector<string> getIDs();
	vector<string> getStrings();
	vector<string> paramToStrings();
	string toString();
	~Predicate();
};