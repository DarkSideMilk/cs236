#include "ruleList.h"

ruleList::ruleList(){}

string ruleList::toString(){
	stringstream output;
	output<<"Rules"<<"("<<list.size()<<"):"<<endl;
	for (unsigned int i = 0; i < list.size(); i++)
		output<<"  "<<list[i].toString()<<"."<<endl;
	return output.str();
}

vector<Rule> ruleList::getList(){
	return list;
}

void ruleList::add(Rule rule){
	list.push_back(rule);
}

ruleList::~ruleList(){}
