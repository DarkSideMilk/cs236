#pragma once
// #include "Rule.h"
#include "Domain.h"

class DatalogProgram
{
private:
	schemeList schemes;
	factList facts;
	queryList queries;
	ruleList rules;

public:
	DatalogProgram();
	string toString();
	void addScheme(Scheme scheme);
	void addFact(Fact fact);
	void addQuery(Query query);
	void addRule(Rule rule);	
	schemeList getSchemes();
	factList getFacts();
	queryList getQueries();
	ruleList getRules();
	~DatalogProgram();
};