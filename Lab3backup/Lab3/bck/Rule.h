#include "Predicate.h"

class Rule
{
private:
	vector<Predicate> ruleList; //make first rule in list base rule
	string colonDash;

public:
	Rule();
	vector<Predicate> getRuleList();
	string getColonDash();
	void addToRuleList(Predicate addThis);
	void setColonDash(string cdash);
	string toString();
	~Rule();
};