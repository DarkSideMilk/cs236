/*
Step 1 - create reccursive decent parser from grammar
Step 2 - classes
Step 3 - put all the pieces together
*/
#pragma once

#include "scanner.h"
#include "DatalogProgram.h"

class parser
{

private:
	vector<token> tokens;
	DatalogProgram grammar;
	Domain domain;
	unsigned int index;
	bool inBounds;
	bool iserror;
	stringstream output;
	char* argv2;

public:

	parser(vector<token> input, char* argv);
	void outputTheFile();	
	void parse();
	void reqListCheck(string type);//required list check
	void predName(string name);
	void pred(Predicate& predi);
	void scheme(Predicate& predi);	
	void fact(Predicate& predi);
	void query(Predicate& predi);	
	Parameter param();	
	void makeRule();
	void predList(Predicate& predi, Rule& newRule);
	void rulePred(Predicate& predi, Rule& newRule);
	void addToGrammar(Predicate predi);
	void addRule(Rule rule);
	void paramList(Predicate& predi);
	string match(string matchMe); //return the value of the token from the scanner output
	void incrIndex();
	void error();
	string toString();
	vector<token> getTokens();
	DatalogProgram getGrammar();
	Domain getDomain();
	unsigned int getIndex();
	stringstream& getOutput();
	~parser();
};