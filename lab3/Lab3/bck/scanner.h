//read char at a time till matches something

#pragma once

#include "token.h"

class scanner
{

public:

		class errorToken
		{
			private:
				string error;
				int line;
			
			public:
				errorToken(int _line);
				int get_Line();
				string getError();
				string toString();
				~errorToken();
		};

	scanner(char* argv);
	vector<token> getTokens();
	void buildMap();
	void scanLine();
	bool cDashCheck(char check);
	void comment(stringstream& scanSpression);
	bool spaceString(stringstream& scanSpression);
	bool scanLoopCheck(stringstream& scanSpression, string& scanString);	
	bool invalidLine(string& expression, ifstream& scanInput);
	void stringScanLoop(stringstream& scanSpression, string& scanString);
	void stringScan(string scanString);
	bool boundsCheck(unsigned int i, unsigned int length);
	bool stringToken(string scanString, stringstream& scanSpression);
	unsigned int endString(unsigned int i, string& tokenAppened, string& scanString, stringstream& scanSpression);
	bool isAfter(unsigned int i, string& scanString, stringstream& scanSpression);
	unsigned int nextString(string& scanString, string& tokenAppened, stringstream& scanSpression, unsigned int i);
	bool newLine(string& scanString);
	bool addStringToken(string& tokenAppened);
	bool isSingleQuote(char is);
	bool nextNotLast(unsigned int i, string scanString);
	int stringBefore(unsigned int i, string& scanString);
	bool stringAfter(unsigned int i, string& scanString);
	int charAdder(unsigned int i, string& scanString);
	void addCreatedString(string& tokenAppened);
	int makeString(string& scanString, string& tokenAppened, unsigned int i);
	bool errorCheck(string scanString, unsigned int i);
	int colonDash(string& scanString, unsigned int i); //0 is nothing, 1 is add 2 to i 2 is exit function
	void charAdd(char check);
	void addToken(string scanString);
	void inputError();
	string toString();
	bool errorString(stringstream& outputString, unsigned int i, unsigned int x);
	~scanner();

private:
	int lineNumber;
	map<char, string> tokenTypes;
	map<string, string> tokenStrings;
	vector<token> output;
	char* input;
	bool isError;
	vector<errorToken> errors;	
};
