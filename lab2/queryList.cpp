#include "queryList.h"

queryList::queryList(){}

vector<Query> queryList::getList(){
	return list;
}

void queryList::add(Query addMe){
	list.push_back(addMe);
}

unsigned int queryList::size(){
	return list.size();
}

string queryList::toString(){
	stringstream output;
	if(list[0].getQuery().getParamList().size() != 0)
		output<<list[0].getQuery().getName()<<"("<<list.size()<<"):"<<endl;
	else{
		output<<list[0].getQuery().getName()<<"(0):"<<endl;
		return output.str();
	}
	
	for(unsigned int i = 0; i < list.size(); i++)
		output<<"  "<<list[i].getQuery().toString()<<"?"<<endl;

	return output.str();
}

queryList::~queryList(){}