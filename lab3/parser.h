#include "scanner.h"
#include "DatalogProgram.h"

class parser{
public:
	vector<token> tokens;
	DatalogProgram grammar;
	set<string> domain;
	unsigned int index;
	bool inBounds;
	bool iserror;
	stringstream output;
	char* argv2;


	parser(vector<token> input, char* argv){
		argv2 = argv;
		tokens = input;
		index = 0;
		inBounds = true;
		iserror = false;
	}

	void outputTheFile(){
		ofstream outputFile(argv2);
		if (outputFile.is_open()){
			if (iserror)
				outputFile << output.str();
			else
				outputFile << toString() << endl;
			outputFile.close();
		}
	}

	void parse(){
		while (index < tokens.size() - 1 && !iserror){
			//schemes
			predName("SCHEMES");
			reqListCheck("SCHEMES");

			//facts
			predName("FACTS");

			//rules
			string check = match("RULES");
			check = match("COLON");
			if (tokens[index].getType() == "ID")//only make rules if it's not an empty list
				makeRule();

			//queries
			predName("QUERIES");
			reqListCheck("QUERIES");
		}

	}

	void reqListCheck(string type){//required list check
		if ((type == "SCHEMES" && grammar.schemes[grammar.schemes.size() - 1].parameterList.size() == 0) ||
			(type == "QUERIES" && grammar.queries[grammar.queries.size() - 1].parameterList.size() == 0)
			){
			cout << "reqListCheck" << endl;
			
			error();
		}
		else
			return;
	}

	void predName(string name){
		Predicate predi;
		predi.name = match(name);
		string check = match("COLON");
		if(tokens[index].getType() == "ID")
			pred(predi);
		else
			addToGrammar(predi);
	}

	void pred(Predicate& predi){
		predi.setID(match("ID"));
		predi.setPunc(match("LEFT_PAREN"));
		paramList(predi);
		predi.setPunc(match("RIGHT_PAREN"));

		scheme(predi);
		fact(predi);
		query(predi);

		//add to grammar
		if (tokens[index].getType() == "ID"){
			Predicate nextPredi;
			nextPredi.setName(predi.getName());
			pred(nextPredi);
		}
	}

	void scheme(Predicate& predi){
		if (predi.getName() == "Schemes"){
			Predicate newScheme(predi);
			//add scheme
			grammar.addScheme(newScheme);
		}
	}

	void fact(Predicate& predi){
		if (predi.getName() == "Facts"){
			string check = match("PERIOD");
			Predicate newFact(predi);
			grammar.addFact(newFact);
		}
		else
			return;
	}

	void query(Predicate& predi){
		if (predi.getName() == "Queries"){
			bool checkQ = false;

			if (index + 1 == tokens.size() - 1)
				checkQ = true;

			predi.setPunc(match("Q_MARK"));

			if (checkQ && tokens[index].getType() == "Q_MARK")
				error();

			Predicate newQuery(predi);
			grammar.addQuery(newQuery);
		}
		else
			return;
	}

	Parameter param(){
		Parameter parame;
		if (tokens[index].getType() == "STRING"){
			//add to domain too
			parame.setName("STRING");
			parame.setValue(match("STRING"));
			domain.insert(parame.getValue());
		}
		else if (tokens[index].getType() == "ID"){
			parame.setName("ID");
			parame.setValue(match("ID"));
		}
		else{
			cout << "param" << endl;
			
			error();
		}

		return parame;
	}

	void makeRule(){
		Rule newRule;
		Predicate predi;
		predi.setName("Rule");

		rulePred(predi, newRule);
		newRule.addToRuleList(predi);
		newRule.setColonDash((match("COLON_DASH")));

		Predicate newPredi;
		newPredi.setName("Rule");
		rulePred(newPredi, newRule);
		newRule.addToRuleList(newPredi);

		Predicate nextPredi;
		nextPredi.setName("Rule");
		predList(nextPredi, newRule);

		string check = match("PERIOD");

		addRule(newRule);

		if (tokens[index].getType() == "ID")
			makeRule();
	}

	void predList(Predicate& predi, Rule& newRule){
		if (tokens[index].getType() == "COMMA"){
			string check = match("COMMA");
			rulePred(predi, newRule);
			newRule.addToRuleList(predi);
			Predicate newPredi;
			newPredi.setName("Rule");
			predList(newPredi, newRule);
		}
	}

	void rulePred(Predicate& predi, Rule& newRule){
		predi.setID(match("ID"));
		predi.setPunc(match("LEFT_PAREN"));
		paramList(predi);
		predi.setPunc(match("RIGHT_PAREN"));
	}

	void addToGrammar(Predicate predi){
		if (predi.name == "Schemes")
			grammar.schemes.push_back(predi);
		else if (predi.name == "Queries")
			grammar.queries.push_back(predi);
		else if (predi.name == "Facts")
			grammar.facts.push_back(predi);
		else{
			cout << "add to grammar" << endl;
			error();
		}
	}

	void addRule(Rule rule){
		grammar.addRule(rule);
	}

	void paramList(Predicate& predi){
		predi.addParam(param());

		if (tokens[index].getType() == "COMMA"){
			string check = match("COMMA");
			paramList(predi);
		}
	}

	string match(string matchMe) //return the value of the token from the scanner output
	{
		string matched = "";

		if(matchMe == tokens[index].type){
			matched = tokens[index].value;
			incrIndex();
		}
		else{
			cout << "match" << endl;
			error();
		}

		return matched;
	}

	void incrIndex(){
		if (index + 1 <= tokens.size() - 1)
			index++;
	}

	void error(){
		cout << "failed here" << endl;
		

		output << "Failure!" << endl << "  " << tokens[index].toString();//<<endl;
		iserror = true;
		throw tokens[index];
	}

	string toString(){
		output << "Success!" << endl << grammar.toString() << toStringDomain();
		return output.str();
	}

	vector<token> getTokens(){
		return tokens;
	}

	DatalogProgram getGrammar(){
		return grammar;
	}

	set<string> getDomain(){
		return domain;
	}

	string toStringDomain(){
		stringstream output;
		output << "Domain(" << domain.size() << "):";

		if (domain.size() == 0)
			return output.str();
		else
			output << endl;

		set<string>::iterator it;
		unsigned int i = 0;
		for (it = domain.begin(); it != domain.end(); it++){
			if (i != domain.size() - 1)
				output << "  '" << *it << "'" << endl;
			else
				output << "  '" << *it << "'";
			i++;
		}
		return output.str();
	}

	unsigned int getIndex(){
		return index;
	}

	stringstream& getOutput(){
		return output;
	}

	~parser(){}

	//parser(vector<token> input, char* argv);
	//void outputTheFile();	
	//void parse();
	//void reqListCheck(string type);//required list check
	//void predName(string name);
	//void pred(Predicate& predi);
	//void scheme(Predicate& predi);	
	//void fact(Predicate& predi);
	//void query(Predicate& predi);	
	//Parameter param();	
	//void makeRule();
	//void predList(Predicate& predi, Rule& newRule);
	//void rulePred(Predicate& predi, Rule& newRule);
	//void addToGrammar(Predicate predi);
	//void addRule(Rule rule);
	//void paramList(Predicate& predi);
	//string match(string matchMe); //return the value of the token from the scanner output
	//void incrIndex();
	//void error();
	//string toString();
	//vector<token> getTokens();
	//DatalogProgram getGrammar();
	//Domain getDomain();
	//unsigned int getIndex();
	//stringstream& getOutput();
	//~parser();
};