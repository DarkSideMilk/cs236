#include "relation.h"

class database:public map<string,relation>{
private:
	vector<relation> relationBase;
	vector<relation> results;
	DatalogProgram data;
	
public:
	database(DatalogProgram newData);
	DatalogProgram getData();
	vector<relation> getRelations();
	void evalSchemes(DatalogProgram data);
	void evalFacts(DatalogProgram data);
	void findTuple(Query query);
	void evalQueries();
	string toString();
	vector<relation> getResults();
	~database();
};