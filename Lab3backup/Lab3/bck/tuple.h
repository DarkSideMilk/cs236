#pragma once
#pragma once
#include <vector>
#include <string>
#include "schema.h"

class Tuple{
public:
	Tuple(){}
	~Tuple(){}
	vector<string> tupleVec;

	void push_back(string addMe){
		tupleVec.push_back(addMe);
	}

	unsigned int size(){
		return tupleVec.size();
	}

	string toString(){
		stringstream ss;
		for(unsigned int i = 0; i < size(); i++)
			ss<<tupleVec[i]<< " ";
		ss<<endl;
		return ss.str();
	}
};
