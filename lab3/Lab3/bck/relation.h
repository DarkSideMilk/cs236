#pragma once
#include "Tuple.h"
class relation: public set<Tuple>{
private:
	string Name;

public:
	relation select(/*relation curRelation,*/ unsigned int index1, unsigned int index2); //index of the tuple, value at each index needs to be equal
	relation select(/*relation curRelation,*/ unsigned int index, string value); //tuple at index would need to equal value to be included in the result
	schema Schema;
	relation project(/*relation curRelation,*/ vector<Parameter> params); //list of indexes
	relation select(/*relation curRelation,*/ vector<Parameter> query);
	relation rename(/*relation curRelation,*/ vector<Parameter> params);
	void setName(string newName);
	void setSchema(vector<Parameter> params);
	void loadFacts(vector<Parameter> params);
	bool dupleCheck(vector<Parameter>& other_list, string check);
	vector<int> findIndexes(vector<Parameter> params);
	string getName();
	schema getSchema();
	string toString();
};