#include "Domain.h"

Domain::Domain(){}

void Domain::add(string addMe){
	domain.insert(addMe);
}

string Domain::toString(){
	stringstream output;
	output<<"Domain("<<domain.size()<<"):";
	
	if(domain.size() == 0)
		return output.str();
	else
		output<<endl;

	set<string>::iterator it;
	unsigned int i=0;
	for(it = domain.begin(); it != domain.end(); it++){
		if(i != domain.size()-1)
			output<<"  '"<<*it<<"'"<<endl;
		else
			output<<"  '"<<*it<<"'";
		i++;
	}
	return output.str();
}

set<string> Domain::getDomain(){
	return domain;
}

Domain::~Domain(){}