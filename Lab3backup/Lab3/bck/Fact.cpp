#include "Fact.h"

Fact::Fact(Predicate _fact){
	fact = _fact;
}

Predicate Fact::getFact(){
	return fact;
}
	
string Fact::toString(){
	return fact.toString();
}

Fact::~Fact(){}