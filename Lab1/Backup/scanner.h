//read char at a time till matches something

#pragma once

#include "token.h"

class scanner
{

	public:

		class errorToken
		{
			public:
				string error;
				int line;

			errorToken(int _line)
			{
				error = "Error on line ";
				line = _line;
			}

			~errorToken(){}
		};

	int lineNumber;
	map<char, string> tokenTypes;
	map<string, string> tokenStrings;
	vector<token> output;
	vector<errorToken> errors;
	char* input;
	bool isError;

	scanner(char* argv)
	{	
		input = argv;
		lineNumber=0;
		isError = false;
	}

	void buildMap()
	{
		tokenTypes[',']="COMMA";
		tokenTypes['.']="PERIOD";
		tokenTypes['?']="Q_MARK";
		tokenTypes['(']="LEFT_PAREN";
		tokenTypes[')']="RIGHT_PAREN";
		tokenTypes[':']="COLON";

		tokenStrings[":-"]="COLON_DASH";
		tokenStrings["Schemes"]="SCHEMES";
		tokenStrings["Facts"]="FACTS";
		tokenStrings["Rules"]="RULES";
		tokenStrings["Queries"]="QUERIES";
	}

	void scanLine()
	{
		ifstream scanInput(input);
		string expression, scanString;

		if(scanInput.is_open())
		{
			buildMap();

			while(getline(scanInput, expression) && !isError)
			{
				stringstream scanSpression;
	            scanSpression<<expression;
	            lineNumber++;
	            if(expression.length()!=0)
	                if(!invalidLine(expression)) //if it's not a commented line
	                    stringScanLoop(scanSpression, scanString);
			}
			scanInput.close();
		}
	}

	bool invalidLine(string expression)
	{
		if(expression.at(0) == '#' || expression.find("EOF") != std::string::npos || 
			expression.find("eof") != std::string::npos || expression.find("END") != std::string::npos || 
			expression.find("end") != std::string::npos)
			return true;
		else
			return false;
	}

	void stringScanLoop(stringstream& scanSpression, string& scanString){
		while(scanSpression>>scanString && !isError)
    		if(!stringToken(scanString, scanSpression))	                    	
            	stringScan(scanString);
	}

	void stringScan(string scanString)
	{	
		string tokenAppened="";	

		unsigned int i=0;
		while(i < scanString.length())
		{
			if(errorCheck(scanString, i))
				return;

			//if input error
			if( signed(i) == -1 || boundsCheck(i, scanString.length()))
				return;

			//normal strings
			i = makeString(scanString, tokenAppened, i);
			
			if(boundsCheck(i, scanString.length()))
				return;
			//characters
			i = charAdder(i, scanString);

			if( signed(i) == -1 || boundsCheck(i, scanString.length()))
				return;
		}
	}

	bool boundsCheck(unsigned int i, unsigned int length){
		if(i >= length)
			return true;
		return false;
	}

	bool stringToken(string scanString, stringstream& scanSpression)
	{
		bool stringFound=false;
		unsigned int i=0;
		string tokenAppened="";

		for(i=0;i<scanString.length();i++)
		{	
			if(isSingleQuote(scanString.at(i)))
			{
				i=stringBefore(i, scanString);
				if(signed(i) == -1)
					return true;

				stringFound=true;
				i++;

				i=endString(i, tokenAppened, scanString, scanSpression);

				if(signed(i) ==-1 )//|| !addStringToken(tokenAppened) || !isAfter(i, scanString, scanSpression) )
					return stringFound;//true
				
				if(!addStringToken(tokenAppened))
					return stringFound; //true
				
				if(!isAfter(i, scanString, scanSpression))
					return stringFound; //true
			}
		}
		return stringFound;
	}

	unsigned int endString(unsigned int i, string& tokenAppened, string& scanString, stringstream& scanSpression){
		while(!isSingleQuote(scanString.at(i)))
		{ //while it isn't the end quote
			tokenAppened += scanString.at(i);
			if(nextNotLast(i, scanString)){
				i=nextString(scanString, tokenAppened, scanSpression, i);
				if(i != 0){
					inputError();
					return -1;
				}
			}
			else
				i++;
		}
		return i;
	}

	bool isAfter(unsigned int i, string& scanString, stringstream& scanSpression)
	{
		if(stringAfter(i, scanString))
			stringToken(scanString, scanSpression);						
		else
			return false;
		return true;
	}

	unsigned int nextString(string& scanString, string& tokenAppened, stringstream& scanSpression, unsigned int i)
	{
		if(scanSpression>>scanString){
			tokenAppened += " ";
			return 0;
		}
		return i;
	}

	bool newLine(string& scanString)
	{
		for(unsigned int i=0;i<scanString.length();i++){
			if(scanString.at(i) == '\n'){
				inputError();
				return true;
			}
		}
		return false;
	}

	bool addStringToken(string& tokenAppened)
	{
		if(newLine(tokenAppened))
		{
			inputError();
			return false;
		}
		else
		{
			// if(tokenAppened.length() > 0 )//|| output.size() > 0)
			output.push_back(token(lineNumber, "STRING", tokenAppened));
			// else	
			// {
			// 	inputError();
			// 	return false;				
			// }
		}
		tokenAppened="";
		return true;
	}

	bool isSingleQuote(char is)
	{
		if(is == '\'')
			return true;
		else
			return false;
	}

	bool nextNotLast(unsigned int i, string scanString)
	{
		if(i+1 >= scanString.length())
			return true;
		else
			return false;
	}

	int stringBefore(unsigned int i, string& scanString)
	{
		if(i!= 0)//erase fun times
		{
			string before="";

			for(unsigned int x=0;x<i;x++)
				before += scanString.at(x);

			if(before.at(0) == '\'')
				return -1;

			stringScan(before);
			scanString.erase(0,i);
			return 0;
		}
		return i;
	}

	bool stringAfter(unsigned int i, string& scanString)
	{
		if(i!= scanString.length()-1)
		{
			string after="";
			unsigned int x;
			for(x=i+1;x<scanString.length() && scanString.at(x)!='\'';x++)
				after += scanString.at(x);
			stringScan(after);
			scanString.erase(0,x); //delete what was after
			if(scanString != ""){
				if(scanString.at(0)=='\'')
					return true;
				else
					return false;
			}

		}
		return false;
	}

	int charAdder(unsigned int i, string& scanString)
	{
		while(!isalpha(scanString.at(i))) //character
		{
			i = colonDash(scanString, i);
			if(signed(i) == -1 || boundsCheck(i, scanString.length()))
				return i;
			
			charAdd(scanString.at(i));
			i++;
			if(i == scanString.length())
				return i;
		}
		return i;
	}

	void addCreatedString(string& tokenAppened)
	{
		if(newLine(tokenAppened))		
			inputError();
		else
			addToken(tokenAppened); //add the created string
		tokenAppened = ""; //reset it
	}

	int makeString(string& scanString, string& tokenAppened, unsigned int i)
	{
		while(isalpha(scanString.at(i)) || isdigit(scanString.at(i)) )//make a string
		{
			tokenAppened += scanString.at(i);
			i++;
			if(i == scanString.length())
			{
				addCreatedString(tokenAppened);
				return i;
			}
		}
		addCreatedString(tokenAppened);
		return i;
	}

	bool errorCheck(string scanString, unsigned int i)
	{
		if(scanString.at(i) == '_' || isdigit(scanString.at(0)))
		{
			inputError();
			return true;
		}
		return false;
	}

	int colonDash(string& scanString, unsigned int i) //0 is nothing, 1 is add 2 to i 2 is exit function
	{
		string tokenAppened="";
		if(i+1 <= scanString.length()-1) //out of bounds check
		{
			if(scanString.at(i) == ':' && scanString.at(i+1) == '-') //colon dash check
			{
				tokenAppened += scanString.at(i);
				i++;
				tokenAppened+= scanString.at(i);
				addToken(tokenAppened);
				i++;
				if(i+1 < scanString.length()) //if the next character is an alpha erase and start over
				{
					if(isalpha(scanString.at(i+1)))
					{
						scanString.erase(0,i);
						stringScan(scanString);
						return -1;
					}
				}
				return i;
			}
		}
		return i;
	}

	void charAdd(char check)
	{
		bool added=false;
        typedef map<char, string>::const_iterator MapIterator;
        for (MapIterator iter = tokenTypes.begin(); iter != tokenTypes.end(); iter++)
        {
        	if(check == iter->first)
        	{
        		string itVal ="";
        		itVal += iter->first;
    			output.push_back( token(lineNumber, iter->second, itVal) );
        		added = true;
        	}
        }
        if(!added && check != '\'')
 			inputError();       	
	}

	void addToken(string scanString)
	{
		bool stringFound=false;
		typedef map<string, string>::const_iterator mIter;
		if(scanString=="")
			return;
        for(mIter it = tokenStrings.begin(); it != tokenStrings.end(); it++){
        	if(scanString == it->first){
				output.push_back(token(lineNumber, it->second, it->first));        	
        		stringFound = true;
        	}
        }
        if(!stringFound && !newLine(scanString))//is ID
        	output.push_back(token(lineNumber, "ID", scanString));
	}

	void inputError()
	{
		errors.push_back(errorToken(lineNumber));
		isError = true;
	}

	string toString()
	{
		unsigned int errorNum = errors.size();
        unsigned int x=0;
        stringstream outputString;

        if(errorNum > 0 && output.size() == 0)
            outputString<<errors[x].error<<errors[x].line<<endl;

        for(unsigned int i=0;i<output.size();i++)
        {
            if(errorNum != 0)
            {//if there are errors
            	outputString<<errors[x].error<<errors[x].line<<endl;
                return outputString.str();
            }
            else
                outputString<<output[i].type<<output[i].value<<output[i].line<<output[i].end<<endl;
        }

        if(output.size()>=0 && errorNum == 0)
            outputString<<"Total Tokens = "<<output.size()<<endl;

        return outputString.str();
	}
	~scanner(){}
};
