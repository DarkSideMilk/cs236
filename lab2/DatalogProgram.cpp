#include "DatalogProgram.h"

DatalogProgram::DatalogProgram(){}

string DatalogProgram::toString(){
	stringstream output;
	output<<schemes.toString()<<facts.toString()<<rules.toString()<<queries.toString();
	return output.str();
}

void DatalogProgram::addScheme(Scheme scheme){
	schemes.add(scheme);
}

void DatalogProgram::addFact(Fact fact){
	facts.add(fact);
}

void DatalogProgram::addQuery(Query query){
	queries.add(query);
}

void DatalogProgram::addRule(Rule rule){
	rules.add(rule);
}

schemeList DatalogProgram::getSchemes(){
	return schemes;
}

factList DatalogProgram::getFacts(){
	return facts;
}

queryList DatalogProgram::getQueries(){
	return queries;
}

ruleList DatalogProgram::getRules(){
	return rules;
}

DatalogProgram::~DatalogProgram(){}