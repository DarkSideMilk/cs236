#include "Parameter.h"

Parameter::Parameter(){}

string Parameter::getName(){
	return name;
}

string Parameter::getValue(){
	return value;
}

void Parameter::setName(string _name){
	name = _name;
}

void Parameter::setValue(string _value){
	value = _value;
}

string Parameter::toString()
{
	if(name == "STRING")
		return "'"+value+"'";
	else
		return value;
}

Parameter::~Parameter(){}	