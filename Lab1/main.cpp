#include "scanner.h"

int main(int c, char *argv[])
{
    scanner meow(argv[1]);
    meow.scanLine();
    
    ofstream outputFile(argv[2]);
	 
    if(outputFile.is_open())
    {
    	outputFile<<meow.toString();
        outputFile.close();
    }
}
