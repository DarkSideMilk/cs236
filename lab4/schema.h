#include "parser.h"

class schema{
public:
	vector<string> attributes;
	
	schema(){}

	void setAttributes(vector<string> _attributes){
		attributes = _attributes;
	}

	void pushBack(string addMe){
		attributes.push_back(addMe);
	}

	void clear(){
		attributes.clear();
	}

	unsigned int size(){
		return attributes.size();
	}

	vector<string> getAttributes(){
		return attributes;
	}

	~schema(){}
};