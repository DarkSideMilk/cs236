#pragma once
#include "Scheme.h"

class schemeList
{

private:
	vector<Scheme> list;

public:
	schemeList();
	vector<Scheme> getList();
	void add(Scheme addMe);
	unsigned int size();
	string toString();
	~schemeList();

	/* data */
};