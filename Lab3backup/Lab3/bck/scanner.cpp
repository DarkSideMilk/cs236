#include "scanner.h"

scanner::errorToken::errorToken(int _line){	
	error = "Error on line ";
	line = _line;
}

int scanner::errorToken::get_Line(){
	return line;
}

string scanner::errorToken::getError(){
	return error;
}

string scanner::errorToken::toString(){
	stringstream outputString;
    outputString<<error<<line<<endl;
    return outputString.str();    	
}

scanner::errorToken::~errorToken(){}

scanner::scanner(char* argv)
{	
	input = argv;
	lineNumber=0;
	isError = false;
}

vector<token> scanner::getTokens(){
	return output;
}

void scanner::buildMap()
{
	tokenTypes[',']="COMMA";
	tokenTypes['.']="PERIOD";
	tokenTypes['?']="Q_MARK";
	tokenTypes['(']="LEFT_PAREN";
	tokenTypes[')']="RIGHT_PAREN";
	tokenTypes[':']="COLON";

	tokenStrings[":-"]="COLON_DASH";
	tokenStrings["Schemes"]="SCHEMES";
	tokenStrings["Facts"]="FACTS";
	tokenStrings["Rules"]="RULES";
	tokenStrings["Queries"]="QUERIES";
}

void scanner::scanLine()
{
	ifstream scanInput(input);
	string expression, scanString;

	if(scanInput.is_open())
	{
		buildMap();
		while(getline(scanInput, expression) && !isError)
		{
			stringstream scanSpression;
            scanSpression<<expression;

            lineNumber++;
            if(expression.length()!=0){
                if(!invalidLine(expression, scanInput) && !isError)//if it's not a commented line
                    stringScanLoop(scanSpression, scanString);
            }
		}
		scanInput.close();
	}
}

bool scanner::invalidLine(string& expression, ifstream& scanInput){
	if(expression.at(0) == '#')
		return true;
	else if(expression.find("EOF") != std::string::npos || 
		expression.find("eof") != std::string::npos || 
		expression.find("END") != std::string::npos || 
		expression.find("end") != std::string::npos)
	{
		if(getline(scanInput, expression)){
			lineNumber++;
			inputError();
		}
		return true;
	}
	else
		return false;
}

void scanner::comment(stringstream& scanSpression)
{
	string expression = scanSpression.str();
	if(expression.find("#") != std::string::npos)
	{
		std::size_t comment = expression.find("#");
		if(comment != std::string::npos)
			expression.erase(comment,expression.length()-1);
		scanSpression.str("");
		scanSpression.clear();
		scanSpression<<expression;
		return;
	}
	return;
}

void scanner::stringScanLoop(stringstream& scanSpression, string& scanString){
	comment(scanSpression);
	if(spaceString(scanSpression))
	{
		scanString = scanSpression.str();
		string traverse="";
		while(scanLoopCheck(scanSpression, scanString)){
			if(!stringToken(scanString, scanSpression))	                    	
        		stringScan(scanString);
        	else
        		return;
        }
	}
	else{
		while(scanLoopCheck(scanSpression, scanString))
			if(!stringToken(scanString, scanSpression))	                    	
        		stringScan(scanString);
	}
}

bool scanner::scanLoopCheck(stringstream& scanSpression, string& scanString){
	if(scanSpression>>scanString && !isError)
		return true;
	return false;
}

bool scanner::spaceString(stringstream& scanSpression){
	unsigned int i = 1;
	string tokenAppened = "";
	string scanString = scanSpression.str();
	if(scanString.length()>1 && isSingleQuote(scanString.at(0)) && isspace(scanString.at(i)))
		return true;
	else
		return false;
}

void scanner::stringScan(string scanString)
{	
	string tokenAppened="";	

	unsigned int i=0;
	while(i < scanString.length())
	{		
		if(errorCheck(scanString, i))
			return;

		//if input error
		if( signed(i) == -1 || boundsCheck(i, scanString.length()))
			return;

		//normal strings
		i = makeString(scanString, tokenAppened, i);
		if(boundsCheck(i, scanString.length()))
			return;
		//characters
		i = charAdder(i, scanString);

		if( signed(i) == -1 || boundsCheck(i, scanString.length()))
			return;
	}
}

bool scanner::boundsCheck(unsigned int i, unsigned int length){
	if(i >= length)
		return true;
	return false;
}

bool scanner::stringToken(string scanString, stringstream& scanSpression)
{
	bool stringFound=false;
	unsigned int i=0;
	string tokenAppened="";

	for(i=0;i<scanString.length();i++){
		if(isSingleQuote(scanString.at(i))){
			i=stringBefore(i, scanString);
			if(signed(i) == -1)
				return true;

			stringFound=true;
			i++;

			i=endString(i, tokenAppened, scanString, scanSpression);
			if(signed(i) ==-1 )
				return stringFound;//true
			
			if(!addStringToken(tokenAppened))
				return stringFound; //true
			if(!isAfter(i, scanString, scanSpression))
				return stringFound; //true
		}
	}
	return stringFound;
}

unsigned int scanner::endString(unsigned int i, string& tokenAppened, string& scanString, stringstream& scanSpression){
	if(scanString.length() == 1)
		i=0;
	while(!isSingleQuote(scanString.at(i))){ //while it isn't the end quote
		tokenAppened += scanString.at(i);
		if(nextNotLast(i, scanString)){
			i=nextString(scanString, tokenAppened, scanSpression, i);
			if(i != 0){
				inputError();
				return -1;
			}
		}
		else
			i++;
	}
	return i;
}

bool scanner::isAfter(unsigned int i, string& scanString, stringstream& scanSpression){
	if(stringAfter(i, scanString))
		stringToken(scanString, scanSpression);						
	else
		return false;
	return true;
}

unsigned int scanner::nextString(string& scanString, string& tokenAppened, stringstream& scanSpression, unsigned int i)
{
	if(scanSpression>>scanString){
		tokenAppened += " ";
		return 0;
	}
	return i;
}

bool scanner::newLine(string& scanString){
	for(unsigned int i=0;i<scanString.length();i++){
		if(scanString.at(i) == '\n'){
			inputError();
			return true;
		}
	}
	return false;
}

bool scanner::addStringToken(string& tokenAppened)
{
	if(newLine(tokenAppened)){
		return false;
	}
	else{
		token addThis(lineNumber, "STRING", tokenAppened);
		output.push_back(addThis);				
	}
	tokenAppened="";
	return true;
}

bool scanner::isSingleQuote(char is){
	if(is == '\'')
		return true;
	else
		return false;
}

bool scanner::nextNotLast(unsigned int i, string scanString){
	if(i+1 >= scanString.length())
		return true;
	else
		return false;
}

int scanner::stringBefore(unsigned int i, string& scanString){
	if(i!= 0)
	{//erase fun times
		string before="";

		for(unsigned int x=0;x<i;x++){	
			if(isspace(scanString.at(x)))
				before += " ";
			else
				before += scanString.at(x);
		}
		if(before.at(0) == '\'')
			return -1;

		stringScan(before);
		scanString.erase(0,i);
		return 0;
	}
	return i;
}

bool scanner::stringAfter(unsigned int i, string& scanString)
{
	if(i!= scanString.length()-1)
	{
		string after="";
		unsigned int x;
		for(x=i+1;x<scanString.length() && scanString.at(x)!='\'';x++)
			after += scanString.at(x);
		stringScan(after);
		scanString.erase(0,x); //delete what was after
		if(scanString != ""){
			if(scanString.at(0)=='\'')
				return true;
			else
				return false;
		}
	}
	return false;
}

int scanner::charAdder(unsigned int i, string& scanString){
	while(!isalpha(scanString.at(i))){ //character
		i = colonDash(scanString, i);

		if(signed(i) == -1 || boundsCheck(i, scanString.length()))
			return i;
		
		charAdd(scanString.at(i));
		i++;
		
		if(i == scanString.length())
			return i;
	}
	return i;
}

void scanner::addCreatedString(string& tokenAppened){
	if(newLine(tokenAppened))
		inputError();
	else
		addToken(tokenAppened); //add the created string
	tokenAppened = ""; //reset it
}

int scanner::makeString(string& scanString, string& tokenAppened, unsigned int i){
	while(isalpha(scanString.at(i)) || isdigit(scanString.at(i)) ){//make a string
		tokenAppened += scanString.at(i);
		i++;
		if(i == scanString.length()){
			addCreatedString(tokenAppened);
			return i;
		}
	}
	addCreatedString(tokenAppened);
	return i;
}

bool scanner::errorCheck(string scanString, unsigned int i){
	if(scanString.at(i) == '_' || isdigit(scanString.at(0))){
		inputError();
		return true;
	}
	return false;
}

int scanner::colonDash(string& scanString, unsigned int i){ //0 is nothing, 1 is add 2 to i 2 is exit function{
	string tokenAppened="";
	if(i+1 <= scanString.length()-1) //out of bounds check
	{
		if(scanString.at(i) == ':' && scanString.at(i+1) == '-') //colon dash check
		{
			tokenAppened += scanString.at(i);
			i++;
			tokenAppened+= scanString.at(i);
			addToken(tokenAppened);
			if(i+1 < scanString.length()) //if the next character is an alpha erase and start over
			{
				if(isalpha(scanString.at(i+1)) || isalpha(scanString.at(i)))
				{
					scanString.erase(0,i);
					stringScan(scanString);
					return -1;
				}
			}
			return i;
		}
	}
	return i;
}

bool scanner::cDashCheck(char check)
{
	if(!output.empty()){
		if(check == '-' && output[output.size()-1].getType() == "COLON_DASH")
			return true;
	}
	return false;
}

void scanner::charAdd(char check)
{
	if(cDashCheck(check))
		return;

	bool added=false;
    
    typedef map<char, string>::const_iterator MapIterator;
    for (MapIterator iter = tokenTypes.begin(); iter != tokenTypes.end(); iter++)
    {

    	if(check == iter->first)
    	{
    		string itVal ="";
    		itVal += iter->first;
    		token addThis(lineNumber, iter->second, itVal);
    		output.push_back(addThis);
    		added = true;
		}
    }
    if(!added && check != '\'')
		inputError();       	
}

void scanner::addToken(string scanString)
{
	bool stringFound=false;
	typedef map<string, string>::const_iterator mIter;
	if(scanString=="")
		return;
	for(mIter it = tokenStrings.begin(); it != tokenStrings.end(); it++)
    {
    	if(scanString == it->first)
    	{
    		token addThis(lineNumber, it->second, it->first);
			output.push_back(addThis);        	
    		stringFound = true;
    	}
    }
    if(!stringFound && !newLine(scanString)){//is ID
    	token addThis(lineNumber, "ID", scanString);
    	output.push_back(addThis);
    }
}

void scanner::inputError(){
	errorToken addThis(lineNumber);
	errors.push_back(addThis);
	isError = true;
}

string scanner::toString()
{
	unsigned int errorNum = errors.size();
    unsigned int x=0;
    stringstream outputString;

    if(errorNum > 0 && output.size() == 0)
        outputString<<errors[x].toString();    

    for(unsigned int i=0;i<output.size();i++){
        if(errorNum != 0){ //if there are errors
        	outputString<<errors[x].toString();
            return outputString.str();
        }
        else
            outputString<<output[i].toString();
    }

    if(output.size()>=0 && errorNum == 0)
        outputString<<"Total Tokens = "<<output.size()<<endl;

    return outputString.str();
}

scanner::~scanner(){}