#include "Tuple.h"

class relation:public set<Tuple>{
private:
	void select(unsigned int index1, unsigned int index2){
		relation newRel;
		newRel.Name = Name;
		newRel.Schema = Schema;
		for (set<Tuple>::iterator it = begin(); it != end(); it++)
		if (it->at(index1) == it->at(index2))
			newRel.insert(*it);
		*this = newRel;
	}

	void select(unsigned int index, string value){
		relation newRel;
		newRel.Name = Name;
		newRel.Schema = Schema;
		for (set<Tuple>::iterator it = begin(); it != end(); it++)
		if (it->at(index) == value)
			newRel.insert(*it);
		*this = newRel;
	}

public:
	string Name;
	schema Schema;

	relation select(vector<Parameter>& params){
		relation newRelation = *this;
		map<string, int> duples;
		for (unsigned int i = 0; i < params.size(); i++){
			if (params[i].getName() == "STRING")
				newRelation.select(i, params[i].getValue()); //delete any Tuples that don't have the duplicate indexes
			else
			if (!(duples.find(params[i].getValue()) == duples.end()))					 												 //constant
				newRelation.select(duples.at(params[i].getValue()), i);
			else
				duples.insert(pair<string, int>(params[i].getValue(), i));
		}
		return newRelation;
	}

	vector<int> findIndexes(vector<Parameter>& params){
		vector<int> indexes;
		vector<Parameter> other_list;
		for (unsigned int i = 0; i<params.size(); i++){
			if (params[i].name != "STRING" && !dupleCheck(other_list, params[i].value)){
				indexes.push_back(i);
				other_list.push_back(params[i]);
			}
		}
		return indexes;
	}

	relation project(vector<Parameter>& params){//sort the variable columns
		vector<int> indexes = findIndexes(params);
		relation newRel;
		newRel.Name = Name;

		for (unsigned int i = 0; i<indexes.size(); i++)
			newRel.Schema.attributes.push_back(Schema.attributes[indexes[i]]);

		for (set<Tuple>::iterator it = begin(); it != end(); it++){
			Tuple newTuple; //Tuple t = *it;
			for (unsigned int i = 0; i<indexes.size(); i++)
				newTuple.push_back(it->at(indexes[i]));
			newRel.insert(newTuple);
		}
		return newRel;
	}

	/*void insert(Tuple newTuple){
		tuples.insert(newTuple);
	}*/


	bool dupleCheck(vector<Parameter>& other_list, string check){
		for (unsigned int i = 0; i<other_list.size(); i++)
		if (check == other_list[i].value)
			return true;
		return false;
	}

	relation rename(vector<Parameter>& params){// relation newRel = *this;
		for (unsigned int i = 0; i<params.size(); i++)
			Schema.attributes[i] = params[i].value;
		return *this;
	}

	string getName(){
		return Name;
	}

	void setName(string newName){
		Name = newName;
	}

	schema getSchema(){
		return Schema;
	}

	void setSchema(vector<Parameter>& params){
		for (unsigned int j = 0; j<params.size(); j++)
			Schema.pushBack(params[j].value);
	}

	void loadFacts(vector<Parameter>& params){
		Tuple newTuple;
		for (unsigned int i = 0; i<params.size(); i++)
			newTuple.push_back(params[i].value);
		insert(newTuple);
	}

	string toString(){
		if (empty())
			return "No\n";

		stringstream ss;
		ss << "Yes(" << size() << ")" << endl;

		if (Schema.size() == 0)
			return ss.str();

		for (set<Tuple>::iterator it = begin(); it != end(); it++){
			ss << "  ";
			for (unsigned int i = 0; i<it->size(); i++){
				ss << Schema.attributes[i] << "=\'" << it->at(i) << "\'";
				if (i != (it->size() - 1))
					ss << ", ";
			}
			ss << endl;
		}
		return ss.str();
	}
};