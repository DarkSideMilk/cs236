#include "Query.h"

Query::Query(Predicate _query){
	query = _query;
}

Predicate Query::getQuery(){
	return query;
}
	
string Query::toString(){
	return query.toString();
}

Query::~Query(){}