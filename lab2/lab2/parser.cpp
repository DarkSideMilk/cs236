#include "parser.h"

parser::parser(vector<token> input, char* argv)
{	
	argv2 = argv;
	tokens = input;
	index=0;
	inBounds = true;
	iserror = false;	
}

void parser::outputTheFile(){
	ofstream outputFile(argv2);
	if(outputFile.is_open()){
		if(iserror)
			outputFile<<output.str();
		else
			outputFile<<toString()<<endl;
		outputFile.close();
	}
}

void parser::parse()
{
	while(index < tokens.size()-1 && !iserror)
	{
		//schemes
		predName("SCHEMES");
		reqListCheck("SCHEMES");

		//facts
		predName("FACTS");

		//rules
		string check = match("RULES");
		check = match("COLON");
		if(tokens[index].getType()=="ID")//only make rules if it's not an empty list
			makeRule();				

		//queries
		predName("QUERIES");
		reqListCheck("QUERIES");
	}
	
}

void parser::reqListCheck(string type){//required list check
	if((type == "SCHEMES" && grammar.getSchemes().getList()[grammar.getSchemes().getList().size()-1].getScheme().getParamList().size() == 0) || 
		(type == "QUERIES" && grammar.getQueries().getList()[grammar.getQueries().getList().size()-1].getQuery().getParamList().size() == 0) 
		){
		error();		
	}
	else
		return;
}

void parser::predName(string name)
{
	Predicate predi;
	predi.setName(match(name));
	string check = match("COLON");
	if(tokens[index].getType() == "ID")
		pred(predi);
	else
		addToGrammar(predi);		
}

void parser::pred(Predicate& predi)	
{
	predi.setID(match("ID"));
	predi.setPunc(match("LEFT_PAREN"));
	paramList(predi);
	predi.setPunc(match("RIGHT_PAREN"));

	scheme(predi);
	fact(predi);
	query(predi);
	
	//add to grammar
	// addToGrammar(predi);
	if(tokens[index].getType() == "ID"){
		Predicate nextPredi;
		nextPredi.setName(predi.getName());	
		pred(nextPredi);
	}
}

void parser::scheme(Predicate& predi){
	if(predi.getName() == "Schemes"){
		Scheme newScheme(predi);
		//add scheme
		grammar.addScheme(newScheme);
	}
}

void parser::fact(Predicate& predi){
	if(predi.getName() == "Facts"){
		string check = match("PERIOD");
		Fact newFact(predi);
		grammar.addFact(newFact);
	}
	else
		return;
}

void parser::query(Predicate& predi){
	if(predi.getName() == "Queries"){
		bool checkQ = false;

		if(index+1 == tokens.size()-1)
			checkQ = true;
	
		predi.setPunc(match("Q_MARK"));

		if(checkQ && tokens[index].getType() == "Q_MARK")
			error();
		
		Query newQuery(predi);
		grammar.addQuery(newQuery);
	}
	else
		return;
}

Parameter parser::param()
{
	Parameter parame;
	if(tokens[index].getType() == "STRING")
	{
		//add to domain too
		parame.setName("STRING");
		parame.setValue(match("STRING"));
		domain.add(parame.getValue());
	}
	else if(tokens[index].getType() == "ID"){
		parame.setName("ID");
		parame.setValue(match("ID"));
	}
	else
		error();

	return parame;
}

void parser::makeRule()
{
	Rule newRule;
	Predicate predi;
	predi.setName("Rule");

	rulePred(predi, newRule);
	newRule.addToRuleList(predi);
	newRule.setColonDash((match("COLON_DASH")));

	Predicate newPredi;
	newPredi.setName("Rule");
	rulePred(newPredi, newRule);
	newRule.addToRuleList(newPredi);

	Predicate nextPredi;
	nextPredi.setName("Rule");
	predList(nextPredi, newRule);

	string check = match("PERIOD");
	
	addRule(newRule);
	
	if(tokens[index].getType() == "ID")
		makeRule();
}

void parser::predList(Predicate& predi, Rule& newRule){				
	if(tokens[index].getType() == "COMMA"){
		string check = match("COMMA");
		rulePred(predi, newRule);
		newRule.addToRuleList(predi);
		Predicate newPredi;
		newPredi.setName("Rule");
		predList(newPredi, newRule);
	}	
}

void parser::rulePred(Predicate& predi, Rule& newRule){		
	predi.setID(match("ID"));
	predi.setPunc(match("LEFT_PAREN"));
	paramList(predi);
	predi.setPunc(match("RIGHT_PAREN"));
}

void parser::addToGrammar(Predicate predi){
	if(predi.getName() == "Schemes")
		grammar.addScheme(predi);
	else if(predi.getName() == "Queries")
		grammar.addQuery(predi);
	else if(predi.getName() == "Facts")
		grammar.addFact(predi);
	else
		error();
}

void parser::addRule(Rule rule){
	grammar.addRule(rule);
}

void parser::paramList(Predicate& predi){
	predi.addParam(param());

	if(tokens[index].getType() == "COMMA"){
		string check = match("COMMA");
		paramList(predi);
	}
}

string parser::match(string matchMe) //return the value of the token from the scanner output
{
	string matched="";

	if(matchMe == tokens[index].getType()){
		matched = tokens[index].getValue();
		incrIndex();
	}
	else
		error();

	return matched;
}

void parser::incrIndex(){
	if(index+1 <= tokens.size()-1)
		index++;
}

void parser::error(){
	output<<"Failure!"<<endl<<"  "<<tokens[index].toString();//<<endl;
	iserror = true;
	throw tokens[index];
}

string parser::toString(){
	output<<"Success!"<<endl<<grammar.toString()<<domain.toString();
	return output.str();
}

vector<token> parser::getTokens(){
	return tokens;
}

DatalogProgram parser::getGrammar(){
	return grammar;
}

Domain parser::getDomain(){
	return domain;
}

unsigned int parser::getIndex(){
	return index;
}

stringstream& parser::getOutput(){
	return output;
}

parser::~parser(){}