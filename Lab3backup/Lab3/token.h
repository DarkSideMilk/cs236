#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <map>
#include <vector>
#include <set>
#include <exception>
#include <cstdlib>
#include <map>
#include <string>

using namespace std;

class token
{

private:
	string type;
	string value;
	int line;
	string end;
	
public:

	token(int lineNumber, string _type, string _value);
	string getType();
	string getValue();
	int getLineNumber();
	string getEnd();
	string toString();
	
	~token();
};
