#pragma once
#include "Fact.h"

class factList
{
private:
	vector<Fact> list;

public:
	factList();
	vector<Fact> getList();
	void add(Fact addMe);
	unsigned int size();
	string toString();
	~factList();

	/* data */
};