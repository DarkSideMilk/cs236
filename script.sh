#!/bin/sh

doit() {
	valgrind --leak-check=full ./lab $1 out.txt
	echo $1 $2
	diff out.txt $2


while true; do 
	read -p "O - Open files ~ Y - Continue" yn
	case $yn in
		[o]* ) kate $1;gedit out.txt;gedit $2;;
		* ) clear; break;;
        esac
done
}


g++ -g -Wall *.cpp -o lab
clear

doit in50.txt out50.txt
doit in54.txt out54.txt
doit in55.txt out55.txt
doit in56.txt out56.txt
doit in58.txt out58.txt
doit in59.txt out59.txt
doit in61.txt out61.txt
doit in62.txt out62.txt
