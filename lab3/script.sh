#!/bin/sh

doit() {
	valgrind --leak-check=full ./lab $1 out.txt
	echo $1 $2
	diff out.txt $2

while true; do 
	read -p "O - Open files ~ Y - Continue" yn
	case $yn in
		[o]* ) gedit $1;gedit out.txt;gedit $2;;
		* ) clear; break;;
        esac
done
}

clear

g++ -g -Wall *.cpp -o lab
doit in41.txt out41.txt
doit in33.txt out33.txt
doit in35.txt out35.txt
doit in36.txt out36.txt
doit in30.txt out30.txt
doit in37.txt out37.txt
doit in40.txt out40.txt
doit in52.txt out52.txt
