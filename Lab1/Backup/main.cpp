#include "scanner.h"

int main(int c, char *argv[])
{
	scanner meow;
    // token rawr;
	ifstream commandFile(argv[1]);
    ofstream outputFile(argv[2]);
    string expression, scanString;

    if(commandFile.is_open() && outputFile.is_open())
    {
        meow.buildMap();
        //input & output
        while(getline(commandFile, expression))
        {   
            stringstream scanSpression;
            scanSpression<<expression;
            meow.lineNumber++;
            if(expression.length()!=0)
            {
                if(expression.at(0) != '#') //if it's not a commented line
                    while(scanSpression>>scanString)
                        meow.stringScan(outputFile, scanString, meow.lineNumber);
            }
        }
        meow.outputStuff(outputFile);
    	commandFile.close();
    	outputFile.close();
    }
}