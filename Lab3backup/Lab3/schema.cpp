#include "schema.h"

schema::schema(){}

void schema::setAttributes(vector<string> _attributes){
	attributes = _attributes;
}

void schema::pushBack(string addMe){
	attributes.push_back(addMe);
}

void schema::clear(){
	attributes.clear();
}

unsigned int schema::size(){
	return attributes.size();
}

vector<string> schema::getAttributes(){
	return attributes;
}

schema::~schema(){}