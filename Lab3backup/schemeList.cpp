#include "schemeList.h"

schemeList::schemeList(){}

vector<Scheme> schemeList::getList(){
	return list;
}

void schemeList::add(Scheme addMe){
	list.push_back(addMe);
}

unsigned int schemeList::size(){
	return list.size();
}

string schemeList::toString()
{
	stringstream output;
	if(list[0].getScheme().getParamList().size() != 0)
		output<<list[0].getScheme().getName()<<"("<<list.size()<<"):"<<endl;
	else{
		output<<list[0].getScheme().getName()<<"(0):"<<endl;
		return output.str();
	}
	
	for(unsigned int i = 0; i < list.size(); i++)
		output<<"  "<<list[i].toString()<<endl;

	return output.str();
}

schemeList::~schemeList(){}