#include "relation.h"
#include "instance.h"

class database: public map<string, relation>{
public:
	vector<relation> results;
	DatalogProgram data;
	unsigned int oldCount;
	unsigned int loopCount;
	bool setuploops;
	map<string, vector<string> > deGraph;
	map<string, set<string> > deGraph2;
	vector<string> qOrder;
	map<string,Node> mapNodes;

	database(DatalogProgram newData){
		data = newData;
		oldCount = 0;
		setuploops = false;
		//loopCount = 0;
	}

	void evalSchemes(){
		for (unsigned int i = 0; i<data.schemes.size(); i++){
			relation newRel;
			newRel.setName(data.schemes[i].identifier);
			newRel.setSchema(data.schemes[i].parameterList);
			this->insert(pair<string, relation>(newRel.Name, newRel));
		}
	}

	void evalFacts(){
		for(unsigned int i = 0; i<data.facts.size(); i++)
			if(data.facts[0].identifier != "")
				this->at(data.facts[i].identifier).loadFacts(data.facts[i].parameterList);
	}

	/*void evalQueries(){
		results = selRenPro(data.queries);
	}*/

	string evalGraph(){
		stringstream out;
		printGraph(out);
		map<string, Node>::iterator mapIt;
		for (unsigned int i = 0; i < qOrder.size(); i++){
			Node d= mapNodes.at(qOrder[i]);
			if (d.ID[0] == 'R')
				break;
			out << data.queries[i].toString();
			out << "?\n\n";

			instance temp;
			int count = 0;
			temp.findPostOrder(&d, count);
			temp.findBackwards();
			printPostOder(out, temp.postOrder, temp.Q);
			printRuleOrder(out, temp.ruleOrder);
			printBackwards(temp.Backwards, out);
			evalRules(temp, out);
			selRenPro();
			printResult(out, i);
			reset();
			out << endl;

		}
		return out.str();
	}

	void evalRules(instance& temp, stringstream& out){
		out << "  Rule Evaluation\n";
		checkTuples();
		do{
			for (unsigned int i = 0; i < temp.ruleOrder.size(); i++){
				stringstream dig;
				for (unsigned int y = 1; temp.ruleOrder[i]->ID[y]; y++)
					dig << temp.ruleOrder[i]->ID[y];
				unsigned int x;
				dig >> x;					

				vector<relation> interm;
				selRenProRule(data.rules[x-1], interm);
				relation newRel1 = joinRelations(interm);
				projectTheRule(newRel1, data.rules[x-1]);
				relation newRel2 = Union(newRel1);
				out << "    " << data.rules[x - 1].toString() << "\n";
			}
			//loopCount++;
		} while (checkTuples() && temp.Backwards.size()!=0);
		out << endl;
	}

	void buildGraph(){
		for (unsigned int i = 0; i < data.queries.size(); i++){
			vector<string> temp;
			set<string> temp2;

			for (unsigned int x = 0; x < data.rules.size(); x++){
				if (data.rules[x].pred().identifier == data.queries[i].identifier){
					stringstream ruleStream;
					ruleStream << "R" << (x + 1);
					temp.push_back(ruleStream.str());
					temp2.insert(ruleStream.str());
				}
 			}
			stringstream qStream;
			qStream << "Q" << (i + 1);
			deGraph.insert(pair<string, vector<string> >(qStream.str(), temp));
			deGraph2.insert(pair<string, set<string> >(qStream.str(), temp2));
			qOrder.push_back(qStream.str());
		}

		for (unsigned int i = 0; i < data.rules.size(); i++){
			vector<string> temp;
			set<string> temp2;
			for (unsigned int x = 1; x < data.rules[i].ruleList.size(); x++){
 				for (unsigned int y = 0; y < data.rules.size(); y++){
					if (data.rules[i].ruleList[x].identifier == data.rules[y].pred().identifier){
						stringstream rStream;
						rStream << "R" << (y + 1);
						temp.push_back(rStream.str());
						temp2.insert(rStream.str());
					}
				}
			}
			stringstream rStream2;
			rStream2 << "R" << (i + 1);
			deGraph.insert(pair<string, vector<string> >(rStream2.str(), temp));
			deGraph2.insert(pair<string, set<string> >(rStream2.str(), temp2));
		}

		makeNode();
	}

	void makeNode(){
		map<string, vector<string> >::iterator p;

		for (p = deGraph.begin(); p != deGraph.end(); p++){
			Node temp;
			temp.ID = p->first;
			mapNodes.insert(pair<string, Node>(temp.ID, temp));
		}

		map<string, Node>::iterator d;
		for (d = mapNodes.begin(); d != mapNodes.end(); d++){
			for (unsigned int s = 0; s < deGraph.at(d->first).size(); s++){
				Node* temp;
				temp = &mapNodes.at(deGraph.at(d->first)[s]);
				d->second.edges.insert(temp);
			}
		}
	}

	void reset(){
		for (map<string, Node>::iterator it = mapNodes.begin(); it != mapNodes.end(); it++){
			it->second.visited = false;
			it->second.order = 0;
		}
	}

	relation Union(relation& unify){
		this->at(unify.Name).insert(unify.begin(), unify.end());
		return this->at(unify.Name);
	}

	void projectTheRule(relation& newRel, Rule& rule){
		for (unsigned int i = 1; i < rule.size(); i++){
			newRel.Name = rule.pred().identifier;
			newRel = newRel.projectRule(rule);
		}
	}

	relation joinRelations(vector<relation>& interm){
		while (interm.size() > 1){
			interm[0] = interm[0].join(interm[1]);
			interm.erase(interm.begin() + 1);
		}
		return interm[0];
	}

	bool checkTuples(){
		unsigned int newCount = 0;
		for (map<string, relation>::iterator it = this->begin(); it != this->end(); it++)
			newCount += it->second.size();
		if (newCount != oldCount){
			oldCount = newCount;
			return true;
		}
		return false;
	}

	void selRenProRule(Rule& rule, vector<relation>& rules){
		for (unsigned int i = 1; i < rule.size(); i++){
			relation temp;
			temp = this->at(rule.ruleList[i].identifier).select(rule.ruleList[i].parameterList);
			temp = temp.rename(rule.ruleList[i].parameterList);
			temp = temp.project(rule.ruleList[i].parameterList);
			rules.push_back(temp);
		}
	}

	void selRenPro(){
		//vector<relation> selects;
		//vector<relation> renames;
		vector<relation> project_Holder;
		for (unsigned int i = 0; i < data.queries.size(); i++){
			//selects.push_back(this->at(preds[i].identifier).select(preds[i].parameterList));		//Select
			//renames.push_back(selects[i].rename(preds[i].parameterList));					//Rename
			//projects.push_back(renames[i].project(preds[i].parameterList));					//Project
			project_Holder.push_back(this->at(data.queries[i].identifier).select(data.queries[i].parameterList).rename(data.queries[i].parameterList).project(data.queries[i].parameterList));					//Project

		}
		results = project_Holder;
	}

	DatalogProgram getData(){
		return data;
	}

	/*string toString(){
		stringstream ss;

	}
*/
	void printGraph(stringstream& out){
		out.clear();
		out << "Dependency Graph\n";
		map<string, set<string> >::iterator setIt;
		for (setIt = deGraph2.begin(); setIt != deGraph2.end(); setIt++){
			out << " " << setIt->first << ":";
			set<string>::iterator x;
			for (x = setIt->second.begin(); x!= setIt->second.end();x++)
				out << " " << *x;
			out << endl;
		}
		out << endl;
	}

	void printPostOder(stringstream& out, set<Node*>& Post, Node* Q){
		out << "  Postorder Numbers\n";

		if (Q)
			out << "    " << Q->ID << ": " << Q->order << "\n";

		for (set<Node*>::iterator P = Post.begin(); P != Post.end(); P++){
			Node* temp = *P;
			out << "    " << temp->ID << ": " << temp->order << endl;
		}
		out << endl;
	}

	void printRuleOrder(stringstream& out, vector<Node*>& rulesO){
		out << "  Rule Evaluation Order\n";
		for (unsigned int i = 0;i < rulesO.size(); i++)
			out << "    " << rulesO[i]->ID << "\n";
		out << endl;
	}	

	void printBackwards(set<Node*>& backO, stringstream& out){
		out << "  Backward Edges\n";
		for (set<Node*>::iterator t = backO.begin(); t != backO.end(); t++){
			Node* temp = *t;
			out << "    " << temp->ID << ":";
			for (set<Node*>::iterator d = temp->edges.begin(); d != temp->edges.end(); d++){
				Node* h = *d;
				if (temp->order <= h->order)
					out << " " << h->ID;
			}
			out << endl;
		}
		out << endl;
	}

	void printResult(stringstream& out, int q){
		out << data.queries[q].toString() << "? " << this->results[q].toString();
	}


	/*string toString(){
		stringstream ss;
		ss << "Schemes populated after "<< loopCount << " passes through the Rules."<<endl;
		for (unsigned int i = 0; i < data.queries.size(); i++)
			ss << data.queries[i].toString() << "? " << results[i].toString();
		ss << "Done!";
		return ss.str();
	}*/

	vector<relation> getResults(){
		return results;
	}

	~database(){}
};