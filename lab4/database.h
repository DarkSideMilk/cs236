#include "relation.h"

class database: public map<string, relation>{
public:
	vector<relation> results;
	DatalogProgram data;
	unsigned int oldCount;
	unsigned int loopCount;


	database(DatalogProgram newData){
		data = newData;
		oldCount = 0;
		loopCount = 0;
	}

	void evalSchemes(){
		for (unsigned int i = 0; i<data.schemes.size(); i++){
			relation newRel;
			newRel.setName(data.schemes[i].identifier);
			newRel.setSchema(data.schemes[i].parameterList);
			this->insert(pair<string, relation>(newRel.Name, newRel));
		}
	}

	void evalFacts(){
		for(unsigned int i = 0; i<data.facts.size(); i++)
			if(data.facts[0].identifier != "")
				this->at(data.facts[i].identifier).loadFacts(data.facts[i].parameterList);
	}

	void evalQueries(){
		results = selRenPro(data.queries);
	}

	void evalRules(){	
		checkTuples();
		do{
			for(unsigned int i = 0; i < data.rules.size(); i++){
				vector<relation> interm;
				selRenProRule(data.rules[i], interm);
				relation newRel1 = joinRelations(interm);
				projectTheRule(newRel1, data.rules[i]);
				relation newRel2 = Union(newRel1);
			}
			loopCount++;
		} while (checkTuples());
	}

	relation Union(relation& unify){
		this->at(unify.Name).insert(unify.begin(), unify.end());
		return this->at(unify.Name);
	}

	void projectTheRule(relation& newRel, Rule& rule){
		for (unsigned int i = 1; i < rule.size(); i++){
			newRel.Name = rule.pred().identifier;
			newRel = newRel.projectRule(rule);
		}
	}

	relation joinRelations(vector<relation>& interm){
		while (interm.size() > 1){
			interm[0] = interm[0].join(interm[1]);
			interm.erase(interm.begin() + 1);
		}
		return interm[0];
	}

	bool checkTuples(){
		unsigned int newCount = 0;
		for (map<string, relation>::iterator it = this->begin(); it != this->end(); it++)
			newCount += it->second.size();
		if (newCount != oldCount){
			oldCount = newCount;
			return true;
		}
		return false;
	}

	void selRenProRule(Rule& rule, vector<relation>& rules){
		for (unsigned int i=1; i < rule.size(); i++){
			relation temp;
			temp = this->at(rule.ruleList[i].identifier).select(rule.ruleList[i].parameterList);
			temp = temp.rename(rule.ruleList[i].parameterList);
			temp = temp.project(rule.ruleList[i].parameterList);
			rules.push_back(temp);
		}
	}

	vector<relation> selRenPro(vector<Predicate>& preds){
		vector<relation> selects;
		vector<relation> renames;
		vector<relation> projects;
		for (unsigned int i = 0; i<preds.size(); i++){
			selects.push_back(this->at(preds[i].identifier).select(preds[i].parameterList));		//Select
			renames.push_back(selects[i].rename(preds[i].parameterList));					//Rename
			projects.push_back(renames[i].project(preds[i].parameterList));					//Project
		}
		return projects;
	}

	DatalogProgram getData(){
		return data;
	}

	string toString(){
		stringstream ss;
		ss << "Schemes populated after "<< loopCount << " passes through the Rules."<<endl;
		for (unsigned int i = 0; i < data.queries.size(); i++)
			ss << data.queries[i].toString() << "? " << results[i].toString();
		ss << "Done!";
		return ss.str();
	}

	vector<relation> getResults(){
		return results;
	}

	~database(){}
};