Success!
Schemes(3):
  f(A)
  f(B)
  f(C)
Facts(3):
  f('1')
  g('2')
  r('3')
Rules(3):
  r(A) :- f(A),g(A)
  f(A) :- r(A)
  g(A) :- f(A),r(A)
Queries(1):
  f('4')
Domain(4):
  '1'
  '2'
  '3'
  '4'
