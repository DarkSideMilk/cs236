#include "Node.h"


class instance{
public:

	set<Node*> edges;
	set<Node*> postOrder;
	set<Node*> Backwards;
	vector<Node*> ruleOrder;
	Node* Q;

	instance(){}

	void findPostOrder(Node* input, int& count){
		input->visited = true;
		for (set<Node*>::iterator it = input->edges.begin(); it != input->edges.end(); it++){
			Node* test = *it;
			if (!test->visited)
				findPostOrder(*it, count);
		}
		
		count++;
		input->order = count;
		
		if (input->ID[0] != 'Q'){
			postOrder.insert(input);
			ruleOrder.push_back(input);
		}
		else
			Q = input;
	}

	void findBackwards(){
		for (set<Node*>::iterator it = postOrder.begin(); it != postOrder.end(); it++){
			Node* Post = *it;
			for (set<Node*>::iterator it2 = Post->edges.begin(); it2 != Post->edges.end(); it2++){
				Node* second = *it2;
				if (Post->order <= second->order)
					Backwards.insert(Post);
			}
		}
	}

	~instance(){}
};