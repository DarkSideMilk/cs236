SK(A,'c')? Yes(2)
  A='a'
  A='b'
SK('c','c')? No
SK('b','c')? Yes(1)
RAWR('m','a','t','t')? Yes(1)
TEST('c')? No
RAWR('m','a','t',x)? Yes(1)
  x='t'
RAWR('m','a',x,x)? Yes(1)
  x='t'
RAWR('m','a',y,x)? Yes(1)
  y='t', x='t'
RAWR('m',x,y,x)? No
TEST('b')? Yes(1)
RAWR('m',y,y,x)? No
RAWR('m',z,y,x)? Yes(1)
  z='a', y='t', x='t'
RAWR(x,z,y,x)? No
RAWR(y,z,y,x)? No
TEST(X)? Yes(3)
  X='a'
  X='b'
  X='d'
RAWR(z,z,y,x)? Yes(1)
  z='t', y='a', x='m'
RAWR(a,z,y,x)? Yes(2)
  a='m', z='a', y='t', x='t'
  a='t', z='t', y='a', x='m'
RAWR('m','e','t','t')? No
SK(X,X)? Yes(1)
  X='b'
SK(A,B)? Yes(3)
  A='a', B='c'
  A='b', B='b'
  A='b', B='c'
TEST('a')? Yes(1)
SK('b',X)? Yes(2)
  X='b'
  X='c'
relativeOf(X,'trapperben')? Yes(2)
  X='meow'
  X='sasquatch'
ancestorOf('trex',Y)? Yes(1)
  Y='raptor'
ancestorOf('trex','trex')? No
childOf('cat','cat')? No
childOf(X,'dog')? Yes(1)
  X='cat'
