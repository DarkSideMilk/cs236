#include "factList.h"

factList::factList(){}

vector<Fact> factList::getList(){
	return list;
}

void factList::add(Fact addMe){
	list.push_back(addMe);
}

unsigned int factList::size(){
	return list.size();
}

string factList::toString(){
	stringstream output;
	if(list[0].getFact().getParamList().size() != 0)
		output<<list[0].getFact().getName()<<"("<<list.size()<<"):"<<endl;
	else{
		output<<list[0].getFact().getName()<<"(0):"<<endl;
		return output.str();
	}
	
	for(unsigned int i = 0; i < list.size(); i++)
		output<<"  "<<list[i].getFact().toString()<<"."<<endl;

	return output.str();
}

factList::~factList(){}