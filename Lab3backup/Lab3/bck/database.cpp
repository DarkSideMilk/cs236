#include "database.h"

database::database(DatalogProgram newData){
	data = newData;
}

void database::evalSchemes(DatalogProgram data){
	for(unsigned int i=0;i<data.getSchemes().getList().size();i++){
		relation newRel;
		newRel.setName(data.getSchemes().getList()[i].getScheme().getID());
		newRel.setSchema(data.getSchemes().getList()[i].getScheme().getParamList());
		this->insert(pair<string,relation>(newRel.getName(),newRel));
	}
}

void database::evalFacts(DatalogProgram data){
	for(unsigned int i=0;i<data.getFacts().getList().size();i++)
		this->at(data.getFacts().getList()[i].getFact().getID()).loadFacts(data.getFacts().getList()[i].getFact().getParamList());
}

void database::findTuple(Query query){
	relation rel;
	for(unsigned int i=0;i<relationBase.size();i++)
		if(query.getQuery().getID() == relationBase[i].getName())
			rel = relationBase[i];
	rel = rel.select(query.getQuery().getParamList());
	rel = rel.rename(query.getQuery().getParamList());
	rel = rel.project(query.getQuery().getParamList());
	results.push_back(rel);
}

void database::evalQueries(){
	vector<relation> selects;
	vector<relation> renames;
	vector<relation> projects;
	for(unsigned int i=0;i<data.getQueries().getList().size();i++){
		selects.push_back(this->at(data.getQueries().getList()[i].getQuery().getID()).select(data.getQueries().getList()[i].getQuery().getParamList()));		//Select
		renames.push_back(selects[i].rename(data.getQueries().getList()[i].getQuery().getParamList()));					//Rename
		projects.push_back(renames[i].project(data.getQueries().getList()[i].getQuery().getParamList()));					//Project
	}
	results = projects;
}

DatalogProgram database::getData(){
	return data;	
}

vector<relation> database::getRelations(){
	return relationBase;
}

string database::toString(){
	stringstream ss;
	for(unsigned int i = 0; i < data.getQueries().getList().size(); i++)
		ss<<data.getQueries().getList()[i].toString()<<"? "<<results[i].toString();
	return ss.str();
}

vector<relation> database::getResults(){
	return results;
}

database::~database(){}