#include "parser.h"

class schema
{
public:
	vector<string> attributes;
	schema();
	void setAttributes(vector<string> _attributes);
	vector<string> getAttributes();
	void pushBack(string addMe);
	unsigned int size();
	void clear();
	~schema();
};