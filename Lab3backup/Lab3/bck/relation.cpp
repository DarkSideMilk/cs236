#include "relation.h"

relation relation::select(vector<Parameter> params){
	relation newRelation = *this;
	map<string,int> duples;
	for(unsigned int i = 0; i < params.size(); i++)				//is it a constant or a variable		
		if(params[i].getName() == "STRING")
			newRelation = select(i, params[i].getValue()); //delete any tuples that don't have the duplicate indexes
		else
			if(!(duples.find(params[i].getValue())==duples.end()))					 												 //constant
				newRelation = select(duples.at(params[i].getValue()), i);		
			else
				duples.insert(pair<string,int>(params[i].getValue(),i));
	return newRelation;
}

relation relation::select(unsigned int index1, unsigned int index2){//select the tuples from the Relation that have the same value in both positions where the variable appears, delete the others
	relation newRel;
	newRel.setName(Name);
	newRel.Schema = Schema;
	for(set<tuple>::iterator it=begin();it!=end();it++)
		if(it->at(index1) == it->at(index2))
			newRel.insert(*it);
	return newRel;
}

relation relation::select(unsigned int index, string value){					//tuple at index would need to equal value to be included in the result
	relation newRel;
	newRel.setName(Name);
	newRel.Schema = Schema;
	for(set<tuple>::iterator it=begin();it!=end();it++)
		if(it->at(index) == value)
			newRel.insert(*it);
	return newRel;
}

relation relation::project(vector<Parameter> params){//sort the variable columns
	vector<int> indexes = findIndexes(params);
	relation newRel;
	newRel.setName(Name);

	for(unsigned int i=0;i<indexes.size();i++)
		newRel.Schema.attributes.push_back(Schema.attributes[indexes[i]]);

	for(set<tuple>::iterator it=begin();it!=end();it++){
		tuple newTuple; //tuple t = *it;
		for(unsigned int i=0;i<indexes.size();i++)	
			newTuple.push_back(it->at(indexes[i]));
		newRel.insert(newTuple);
	}
	return newRel;	
}

vector<int> relation::findIndexes(vector<Parameter> params){
	vector<int> index;
	vector<Parameter> other_list;
	for(unsigned int i=0;i<params.size();i++){
		if(params[i].getName() != "STRING" && !dupleCheck(other_list,params[i].getValue())){
			index.push_back(i);
			other_list.push_back(params[i]);
		}
	}
	return index;
}

bool relation::dupleCheck(vector<Parameter>& other_list, string check){
	for(unsigned int i=0;i<other_list.size();i++)
		if(check == other_list[i].getValue())
			return true;
	return false;
}

relation relation::rename(vector<Parameter> params){
	relation newRel = *this;
	for(unsigned int i=0;i<params.size();i++)
		newRel.Schema.attributes[i] = params[i].getValue();
	return newRel;
}

string relation::getName(){
	return Name;
}

void relation::setName(string newName){
	Name = newName;
}

schema relation::getSchema(){
	return Schema;
}

void relation::setSchema(vector<Parameter> params){
	for(unsigned int j=0;j<params.size();j++)
		Schema.pushBack(params[j].getValue());
}

void relation::loadFacts(vector<Parameter> params){
	tuple newTuple;
	for(unsigned int i=0;i<params.size();i++)
		newTuple.push_back(params[i].getValue());
	insert(newTuple);
}


string relation::toString(){
	if(size() == 0)
		return "No\n";
	
	stringstream ss;
	ss<<"Yes("<<size()<<")"<<endl;

	if(Schema.size()==0)
		return ss.str();
	
	for(set<tuple>::iterator it=begin(); it!=end(); it++){
		ss<<"  ";
		for(unsigned int i=0; i<it->size();i++){
			ss<<Schema.attributes[i]<<"=\'"<<it->at(i)<<"\'";
			if(i!=(it->size()-1))
				ss<<", ";
		}
		ss<<endl;
	}
	return ss.str();
}
