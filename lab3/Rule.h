#include "Predicate.h"

class Rule{

public:
	vector<Predicate> ruleList; //make first rule in list base rule
	string colonDash;
	
	Rule(){}

	vector<Predicate> getRuleList(){
		return ruleList;
	}
	
	string getColonDash(){
		return colonDash;
	}

	void addToRuleList(Predicate addThis){
		ruleList.push_back(addThis);
	}

	void setColonDash(string cdash){
		colonDash = cdash;
	}

	string toString(){
		if (ruleList.size() == 0)
			return "";
		unsigned int i = 0;
		stringstream output;
		output << ruleList[i].toString() << " " << colonDash << " ";
		for (i = 1; i < ruleList.size(); i++){
			if (i != ruleList.size() - 1)
				output << ruleList[i].toString() << ",";
			else
				output << ruleList[i].toString();
		}
		return output.str();
	}

	~Rule(){}


	/*Rule();
	vector<Predicate> getRuleList();
	string getColonDash();
	void addToRuleList(Predicate addThis);
	void setColonDash(string cdash);
	string toString();
	~Rule();*/
};