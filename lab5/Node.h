#include "token.h"

class Node{

public:

	string ID;
	bool visited;
	int order;
	int ID_num;
	set<Node*> edges;

	Node(){
		ID = "";
		visited = false;
		order = 0;
		ID_num = 0;
	}

	bool operator<(const Node& rhs) const{
		return (ID < rhs.ID);
	}

	~Node(){}

};