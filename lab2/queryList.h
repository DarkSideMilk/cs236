#pragma once
#include "Query.h"

class queryList
{
private:
	vector<Query> list;

public:
	queryList();
	vector<Query> getList();
	void add(Query addMe);
	unsigned int size();
	string toString();
	~queryList();
};