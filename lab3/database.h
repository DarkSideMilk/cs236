#include "relation.h"

class database: public map<string, relation>{
public:
	vector<relation> results;
	DatalogProgram data;

	database(DatalogProgram newData){
		data = newData;
	}

	void evalSchemes(){
		for (unsigned int i = 0; i<data.schemes.size(); i++){
			relation newRel;
			newRel.setName(data.schemes[i].identifier);
			newRel.setSchema(data.schemes[i].parameterList);
			this->insert(pair<string, relation>(newRel.Name, newRel));
		}
	}

	void evalFacts(){
		for(unsigned int i = 0; i<data.facts.size(); i++)
			if(data.facts[0].identifier != "")
				this->at(data.facts[i].identifier).loadFacts(data.facts[i].parameterList);
	}

	void evalQueries(){
		vector<relation> selects;
		vector<relation> renames;
		vector<relation> projects;
		for (unsigned int i = 0; i<data.queries.size(); i++){
			selects.push_back(this->at(data.queries[i].identifier).select(data.queries[i].parameterList));		//Select
			renames.push_back(selects[i].rename(data.queries[i].parameterList));					//Rename
			projects.push_back(renames[i].project(data.queries[i].parameterList));					//Project
		}
		results = projects;
	}

	DatalogProgram getData(){
		return data;
	}

	string toString(){
		stringstream ss;
		for (unsigned int i = 0; i < data.queries.size(); i++)
			ss << data.queries[i].toString() << "? " << results[i].toString();
		return ss.str();
	}

	vector<relation> getResults(){
		return results;
	}

	~database(){}
};