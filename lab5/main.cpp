#include "database.h"

int main(int c, char *argv[]){
	scanner meow(argv[1]);
	meow.scanLine();

	parser rawr(meow.output, argv[2]);
	ofstream output(argv[2]);
	try{
		if(output.is_open()){
			rawr.parse();
			DatalogProgram data = rawr.getGrammar();
			database dBase(data);
			dBase.evalSchemes();
			dBase.evalFacts();
			dBase.buildGraph();
			output << dBase.evalGraph();
			//dBase.evalQueries();
			//output << dBase.toString();
			output.close();
		}			
	}
	catch(token e){
		rawr.outputTheFile();	
	}
}	
