#include "Tuple.h"

class relation{
private:
	string Name;

public:
	set<Tuple> tuples;
	schema Schema;

	relation();
	~relation();
	relation select(unsigned int index1, unsigned int index2); //index of the tuple, value at each index needs to be equal
	relation select(unsigned int index, string value); //tuple at index would need to equal value to be included in the result
	void insert(Tuple newTuple);
	unsigned int size();
	relation project(vector<Parameter> params); //list of indexes
	relation select(vector<Parameter> query);
	relation rename(vector<Parameter> params);
	void setName(string newName);
	void setSchema(vector<Parameter> params);
	void loadFacts(vector<Parameter> params);
	bool dupleCheck(vector<Parameter>& other_list, string check);
	vector<int> findIndexes(vector<Parameter> params);
	string getName();
	schema getSchema();
	string toString();
};