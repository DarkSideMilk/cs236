#include "tuple.h"

tuple::tuple();
~tuple();
vector<string> tupleVec;
void push_back(string addMe);
unsigned int size();

string tuple::toString(){
	stringstream ss;
	for (unsigned int i = 0; i < this->size(); i++)
		ss << at(i) << " ";
	ss << endl;
	return ss.str();
}
