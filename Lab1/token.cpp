#include "token.h"

token::token(int lineNumber, string _type, string _value)
{
	type=_type;
	value=_value;
	line=lineNumber;
}

string token::getType(){
	return type;
}

string token::getValue(){
	return value;
}

int token::getLineNumber(){
	return line;
}

string token::toString()
{
	stringstream outputString;
    outputString<<"("<<type<<",\""<<value<<"\","<<line<<")"<<endl;
    return outputString.str();
}

token::~token(){}
