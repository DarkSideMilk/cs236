#include "token.h"

class Parameter
{

private:
	string name; //string or id
	string value;

public:

	Parameter();
	string getName();
	string getValue();
	void setName(string _name);
	void setValue(string _value);
	string toString();
	~Parameter();
};