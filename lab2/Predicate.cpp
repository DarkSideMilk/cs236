#include "Predicate.h"

Predicate::Predicate(){}

void Predicate::setName(string _name){
	name = _name;
}

void Predicate::setID(string _id){
	identifier = _id;
}

void Predicate::setPunc(string punc){
	if(punc == "(")
		Lparen = punc;
	else if(punc == ")")
		Rparen = punc;
	else if(punc == "?")
		q_mark = punc;
	else
		return;
}

void Predicate::addParam(Parameter param){
	parameterList.push_back(param);
}

string Predicate::getID(){
	return identifier;
}

string Predicate::getName(){
	return name;
}

string Predicate::getQmark(){
	return q_mark;
}

string Predicate::getLparen(){
	return Lparen;
}

string Predicate::getRparen(){
	return Rparen;
}

vector<Parameter> Predicate::getParamList(){
	return parameterList;
}

string Predicate::toString()
{
	if(parameterList.size()==0)
		return "";
	
	stringstream output;
	
	output<<identifier<<Lparen; 
	
	for(unsigned int i = 0; i < parameterList.size(); i++){
		if(i != parameterList.size()-1)
			output<<parameterList[i].toString()<<",";
		else
			output<<parameterList[i].toString()<<Rparen;
	}
	return output.str();
}

Predicate::~Predicate(){}