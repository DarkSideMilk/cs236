#include "schema.h"

class Tuple: public vector<string> {
public:
	string toString(){
		stringstream ss;
		for (unsigned int i = 0; i < this->size(); i++)
			ss << at(i) << " ";
		ss << endl;
		return ss.str();
	}
};