#include "Rule.h"

Rule::Rule(){}

vector<Predicate> Rule::getRuleList(){
	return ruleList;
}

string Rule::getColonDash(){
	return colonDash;
}

void Rule::addToRuleList(Predicate addThis){
	ruleList.push_back(addThis);
}

void Rule::setColonDash(string cdash){
	colonDash = cdash;
}

string Rule::toString(){
	if(ruleList.size()==0)
		return "";
	unsigned int i=0;
	stringstream output;
	output<<ruleList[i].toString()<<" "<<colonDash<<" ";
	for (i = 1; i < ruleList.size(); i++){
		if(i != ruleList.size()-1)
			output<<ruleList[i].toString()<<",";
		else
			output<<ruleList[i].toString();				
	}
	return output.str();
}

Rule::~Rule(){}
