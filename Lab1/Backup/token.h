#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <map>
#include <vector>

using namespace std;

class token
{

public:
	string type;
	string value;
	int line;
	string end;

	token(int lineNumber, string _type, string _value)
	{
		type="("+_type+",";
		value="\""+_value+"\",";
		line=lineNumber;
		end = ")";
	}
	
	~token(){}
};