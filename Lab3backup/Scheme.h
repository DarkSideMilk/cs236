#pragma once
#include "Rule.h"

class Scheme
{
private:
	Predicate scheme;

public:
	Scheme(Predicate _scheme);
	Predicate getScheme();
	string toString();
	~Scheme();

	/* data */
};