#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <map>
#include <vector>
#include <set>
#include <exception>
#include <cstdlib>
#include <map>
#include <string>

using namespace std;

class token{
public:

	string type;
	string value;
	int line;
	string end;

	token(int lineNumber, string _type, string _value){
		type = _type;
		value = _value;
		line = lineNumber;
	}

	string getType(){
		return type;
	}

	string getValue(){
		return value;
	}

	int getLineNumber(){
		return line;
	}

	string toString(){
		stringstream outputString;
		outputString << "(" << type << ",\"" << value << "\"," << line << ")" << endl;
		return outputString.str();
	}

	~token(){}

/*
	token(int lineNumber, string _type, string _value);
	string getType();
	string getValue();
	int getLineNumber();
	string getEnd();
	string toString();
	
	~token();*/
};
