#include "token.h"

class Parameter{
public:
	string name; //string or id
	string value;

	Parameter(){}

	string getName(){
		return name;
	}

	string getValue(){
		return value;
	}

	void setName(string _name){
		name = _name;
	}

	void setValue(string _value){
		value = _value;
	}

	string toString(){
		if (name == "STRING")
			return "'" + value + "'";
		return value;
	}

	~Parameter(){}

	/*Parameter();
	string getName();
	string getValue();
	void setName(string _name);
	void setValue(string _value);
	string toString();
	~Parameter();*/
};