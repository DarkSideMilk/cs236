#include "database.h"

int main(int c, char *argv[]){
	scanner meow(argv[1]);
	meow.scanLine();
	parser rawr(meow.getTokens(), argv[2]);
	ofstream output(argv[2]);
	DatalogProgram data;
	try{
		if(output.is_open()){	
			rawr.parse();
			// cout<<rawr.toString();
			data = rawr.getGrammar();
			database dBase(data);
			dBase.evalSchemes(data);
			dBase.evalFacts(data);
			dBase.evalQueries();
			output << dBase.toString();
			output.close();
		}			
	}
	catch(token e){
		rawr.outputTheFile();	
	}
}	
