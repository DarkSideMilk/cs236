#include "Tuple.h"

class relation:public set<Tuple>{
private:
	void select(unsigned int index1, unsigned int index2){
		relation newRel;
		newRel.Name = Name;
		newRel.Schema = Schema;
		for (set<Tuple>::iterator it = begin(); it != end(); it++)
		if (it->at(index1) == it->at(index2))
			newRel.insert(*it);
		*this = newRel;
	}

	void select(unsigned int index, string value){
		relation newRel;
		newRel.Name = Name;
		newRel.Schema = Schema;
		for (set<Tuple>::iterator it = begin(); it != end(); it++)
		if (it->at(index) == value)
			newRel.insert(*it);
		*this = newRel;
	}

public:
	string Name;
	schema Schema;

/*	Translate each rule into a query using the following five steps.


		5. Union the results of Step 4 with the relation in the database whose name is equal to the name 
		of the head of the rule.In Step 4 we called this relation in the database, r.Add tuples to relation r 
		from the result of Step 4. */

	//relation Union(relation& unify){
	//	unify.insert(this->begin(), this->end());
	//	return unify;
	//}

	relation join(relation& joinThis){
		relation newRel;
		set<Tuple>::iterator it1, it2;
		vector < pair <int, int> > joinplicates = findJoinplicates(joinThis);
		for(it1 = this->begin(); it1 != this->end(); it1++){
			Tuple curT1 = *it1;
			for (it2 = joinThis.begin(); it2 != joinThis.end(); it2++){
				Tuple curT2 = *it2;
				if (joinCheck(joinplicates, curT1, curT2)){
					Tuple temp = curT1;
					temp.insert(temp.end(), curT2.begin(), curT2.end());
					newRel.insert(temp);
				}
			}
		}
		newRel.Schema = joinSchema(this->Schema, joinThis.Schema);

		return newRel;
	}

	schema joinSchema(schema appendedSchema, schema jointSchema){
		for (unsigned int i = 0; i < jointSchema.size(); i++)
			appendedSchema.pushBack(jointSchema.attributes[i]);
		return appendedSchema;
	}

	vector< pair<int, int> > findJoinplicates(relation& joinThis){
		vector< pair<int, int> > joinplicate;
		for (unsigned int i = 0; i < this->Schema.size(); i++)
			for (unsigned int y = 0; y < joinThis.Schema.size(); y++)
				if (this->Schema.attributes[i] == joinThis.Schema.attributes[y])
					joinplicate.push_back(make_pair(i, y));
		return joinplicate;
	}
	
	bool joinCheck(vector< pair<int,int> >& joinplicate, Tuple& t1, Tuple& t2){
		for (unsigned int i = 0; i < joinplicate.size(); i++)
			if ( t1[joinplicate[i].first] != t2[joinplicate[i].second])
				return false;
		return true;
	}

	relation select(vector<Parameter>& params){
		relation newRelation = *this;
		map<string, int> duples;
		for (unsigned int i = 0; i < params.size(); i++){
			if (params[i].getName() == "STRING")
				newRelation.select(i, params[i].getValue()); //delete any Tuples that don't have the duplicate indexes
			else
			if (!(duples.find(params[i].getValue()) == duples.end()))					 												 //constant
				newRelation.select(duples.at(params[i].getValue()), i);
			else
				duples.insert(pair<string, int>(params[i].getValue(), i));
		}
		return newRelation;
	}

	vector<int> findIndexes(vector<Parameter>& params){
		vector<int> indexes;
		vector<Parameter> other_list;
		for (unsigned int i =0; i<params.size(); i++){
			if (params[i].name != "STRING" && !dupleCheck(other_list, params[i].value)){
				indexes.push_back(i);
				other_list.push_back(params[i]);
			}
		}
		return indexes;
	}

	relation project(vector<Parameter>& params){//sort the variable columns
		vector<int> indexes = findIndexes(params);
		relation newRel;
		newRel.Name = Name;

		for (unsigned int i = 0; i<indexes.size(); i++)
			newRel.Schema.attributes.push_back(Schema.attributes[indexes[i]]);

		for(set<Tuple>::iterator it = this->begin(); it != this->end(); it++){
			Tuple newTuple; //Tuple t = *it;
			for(unsigned int y = 0;y<indexes.size(); y++)
				newTuple.push_back(it->at(indexes[y]));
			newRel.insert(newTuple);
		}
		return newRel;
	}

	vector<int> findRuleIndexes(Rule& rule){
		vector<int> indexes;
		vector<Parameter> otherList;
		for (unsigned int i = 0; i < rule.pred().parameterList.size(); i++){
			for (unsigned int y = 0; y < Schema.size(); y++){
				if (rule.pred().parameterList[i].value == Schema.attributes[y] && !dupleCheck(otherList, Schema.attributes[y])){
					indexes.push_back(y);
					otherList.push_back(rule.pred().parameterList[i]);
				}
			}
		}
		return indexes;
	}

	relation projectRule(Rule& rule){
		vector<int> indexes = findRuleIndexes(rule);
		relation newRel;
		newRel.Name = Name;

		for (unsigned int i = 0; i<indexes.size(); i++)
			newRel.Schema.attributes.push_back(Schema.attributes[indexes[i]]);

		for (set<Tuple>::iterator it = this->begin(); it != this->end(); it++){
			Tuple newTuple; 
			for (unsigned int y = 0; y<indexes.size(); y++)
				newTuple.push_back(it->at(indexes[y]));
			newRel.insert(newTuple);
		}
		return newRel;
	}

	bool dupleCheck(vector<Parameter>& other_list, string check){
		for (unsigned int i = 0; i<other_list.size(); i++)
		if (check == other_list[i].value)
			return true;
		return false;
	}

	relation rename(vector<Parameter>& params){// relation newRel = *this;
		for (unsigned int i = 0; i<params.size(); i++)
			Schema.attributes[i] = params[i].value;
		return *this;
	}

	string getName(){
		return Name;
	}

	void setName(string newName){
		Name = newName;
	}

	schema getSchema(){
		return Schema;
	}

	void setSchema(vector<Parameter>& params){
		for (unsigned int j = 0; j<params.size(); j++)
			Schema.pushBack(params[j].value);
	}

	void loadFacts(vector<Parameter>& params){
		Tuple newTuple;
		for (unsigned int i = 0; i<params.size(); i++)
			newTuple.push_back(params[i].value);
		insert(newTuple);
	}

	string toString(){
		if (empty())
			return "No\n";
		stringstream ss;
		ss << "Yes(" << size() << ")" << endl;
		if (Schema.size() == 0)
			return ss.str();
		for (set<Tuple>::iterator it = begin(); it != end(); it++){
			ss << "  ";
			for (unsigned int i = 0; i<it->size(); i++){
				ss << Schema.attributes[i] << "=\'" << it->at(i) << "\'";
				if (i != (it->size() - 1))
					ss << ", ";
			}
			ss << endl;
		}
		return ss.str();
	}
};