#include "ruleList.h"

class Domain
{
private:
	set<string> domain;

public:
	Domain();
	void add(string addMe);
	string toString();
	set<string> getDomain();
	~Domain();

	/* data */
};