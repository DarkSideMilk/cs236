#pragma once
#include "factList.h"

class Query{
private:
	Predicate query;

public:
	Query(Predicate _query);
	Predicate getQuery();
	string toString();
	~Query();

	/* data */
};