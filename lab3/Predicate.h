#include "Parameter.h"

class Predicate{
public:

	string identifier;
	string name;
	string Lparen, Rparen;
	string q_mark;
	vector<Parameter> parameterList;

	Predicate(){}

	void setName(string _name){
		name = _name;
	}

	void setID(string _id){
		identifier = _id;
	}

	void setPunc(string punc){
		if (punc == "(")
			Lparen = punc;
		else if (punc == ")")
			Rparen = punc;
		else if (punc == "?")
			q_mark = punc;
		else
			return;
	}

	void addParam(Parameter param){
		parameterList.push_back(param);
	}

	string getID(){
		return identifier;
	}

	string getName(){
		return name;
	}

	string getQmark(){
		return q_mark;
	}

	string getLparen(){
		return Lparen;
	}

	string getRparen(){
		return Rparen;
	}

	vector<Parameter> getParamList(){
		return parameterList;
	}

	vector<string> getIDs(){
		vector<string> idz;
		if (parameterList[0].getName() == "ID")
		for (unsigned int i = 0; i < parameterList.size(); i++)
			idz.push_back(parameterList[i].getValue());
		return idz;
	}

	vector<string> getStrings(){
		vector<string> stringz;
		if (parameterList[0].getName() == "STRING")
		for (unsigned int i = 0; i < parameterList.size(); i++)
			stringz.push_back(parameterList[i].getValue());
		return stringz;
	}

	vector<string> paramToStrings(){
		vector<string> paramz;
		for (unsigned int i = 0; i < parameterList.size(); i++)
			paramz.push_back(parameterList[i].getValue());
		return paramz;
	}

	string toString(){
		if (parameterList.size() == 0)
			return "";

		stringstream output;

		output << identifier << Lparen;

		for (unsigned int i = 0; i < parameterList.size(); i++){
			if (i != parameterList.size() - 1)
				output << parameterList[i].toString() << ",";
			else
				output << parameterList[i].toString() << Rparen;
		}
		return output.str();
	}

	~Predicate(){}

	/*Predicate();
	void setName(string _name);
	void setID(string _id);
	void setPunc(string punc);
	void addParam(Parameter param);
	string getID();
	string getName();
	string getQmark();
	string getLparen();
	string getRparen();
	vector<Parameter> getParamList();
	vector<string> getIDs();
	vector<string> getStrings();
	vector<string> paramToStrings();
	string toString();
	~Predicate();*/
};