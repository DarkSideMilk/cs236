#include "relation.h"

class database{//: public map<string, relation>{
private:
	// vector<relation> relationBase;
	
public:
	vector<relation> results;
	DatalogProgram data;
	map<string, relation> relations;

	database(DatalogProgram newData);
	DatalogProgram getData();
	// vector<relation> getRelations();
	void evalSchemes(DatalogProgram data);
	void evalFacts(DatalogProgram data);
	void evalQueries();
	string toString();
	void insert(pair<string,relation> addMe);

	vector<relation> getResults();
	~database();
};

/*
	string evalQueries(vector<Query>);
		string evalQuery(Query);
			Relation evalPredicate(Predicate);
				result = result.select(int, string);
				result = result.select(int, int);
				result = result.project(vector<int>);
				result = result.rename(in, string);
				return result;
			return result.toString();
		str += evalQuery
		return str;

*/