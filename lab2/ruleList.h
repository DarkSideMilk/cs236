#pragma once
#include "queryList.h"

class ruleList
{
private:
	vector<Rule> list;

public:
	ruleList();
	string toString();
	vector<Rule> getList();
	void add(Rule rule);
	~ruleList();
};