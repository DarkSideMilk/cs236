#pragma once
#include "schemeList.h"

class Fact
{
private:
	Predicate fact;

public:
	Fact(Predicate _fact);
	Predicate getFact();
	string toString();
	~Fact();

	/* data */
};