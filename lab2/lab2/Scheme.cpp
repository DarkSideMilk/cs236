#include "Scheme.h"

Scheme::Scheme(Predicate _scheme){
	scheme = _scheme;
}

Predicate Scheme::getScheme(){
	return scheme;
}

string Scheme::toString(){
	return scheme.toString();
}

Scheme::~Scheme(){}