#include "Tuple.h"

string Tuple::toString(){
	stringstream ss;
	for (unsigned int i = 0; i < this->size(); i++)
		ss << at(i) << " ";
	ss << endl;
	return ss.str();
}